import requests
import json
from faker import Faker
from flask import render_template, request, redirect
from models import Author, Genre, Book
from forms import AuthorForm, GenreForm, BookForm
from settings import app


@app.route('/home/')
def render_home_page():
    return render_template('home.html')


@app.route('/requirements/')
def displays_file_content():
    with open('requirements.txt') as r:
        data = r.readlines()
        data = [line.strip('\n') for line in data]
        return render_template('package.html', packages=data)


@app.route('/generate-users/')
def generate_users():
    fake = Faker(['en_GB', 'ru_RU', 'de_DE', 'en_US', 'uk_UA'])
    count_of_users_to_display = 100
    if 'value' in request.values:
        count_of_users_to_display = abs(int(request.values['value']))
    names_and_emails = [fake.first_name() + ' ' + fake.company_email() for _ in range(100)]
    return render_template('user.html', users=names_and_emails[:count_of_users_to_display])


@app.route('/space/')
def men_in_space():
    data = requests.get('http://api.open-notify.org/astros.json')
    json_to_python = json.loads(data.text)
    return render_template('astronaut.html', men=json_to_python)


@app.route('/')
def library():
    books = Book.query.order_by(Book.id.desc())
    return render_template('library_alch.html', books=books)


@app.route('/year/<int:year>/')
def books_by_year(year):
    books = Book.query.filter_by(year=year).all()
    return render_template('books_by_year_alch.html', books=books)


@app.route('/genre/<int:pk>/')
def books_by_genre(pk):
    books = Book.query.filter_by(genre_id=pk).all()
    return render_template('books_by_genre_alch.html', books=books)


@app.route('/create/author/', methods=['GET', 'POST'])
def create_author():
    form = AuthorForm(request.form)
    if request.method == 'POST' and form.validate():
        form.save()
        return redirect('/')

    return render_template('author_alch.html', form=form)


@app.route('/create/genre/', methods=['GET', 'POST'])
def create_genre():
    form = GenreForm(request.form)
    list_of_genres = Genre.query.all()
    if request.method == 'POST' and form.validate():
        for genre in list_of_genres:
            if form.data['name'] == genre.name:
                break
        else:
            form.save()
            return redirect('/')

    return render_template('genre_pw.html', genres=list_of_genres, form=form)


@app.route('/create/book/', methods=['GET', 'POST'])
def create_book():
    form = BookForm(request.form)
    form.author.choices = [(author.id, author.name) for author in Author.query.all()]
    form.genre.choices = [(genre.id, genre.name) for genre in Genre.query.all()]
    if request.method == 'POST' and form.validate():
        form.save()
        return redirect('/')

    return render_template('create_book_alch.html', form=form)


@app.route('/update/book/<int:book_id>/', methods=['GET', 'POST'])
def update_book(book_id):
    form = BookForm(request.form)
    form.author.choices = [(author.id, author.name) for author in Author.query.all()]
    form.genre.choices = [(genre.id, genre.name) for genre in Genre.query.all()]
    if request.method == 'POST' and form.validate():
        form.update(book_id)
        return redirect('/')

    book = Book.query.filter_by(id=book_id).first()
    form = BookForm(name=book.name, author=book.author.id, genre=book.genre.id, year=book.year)
    form.author.choices = [(author.id, author.name) for author in Author.query.all()]
    form.genre.choices = [(genre.id, genre.name) for genre in Genre.query.all()]
    return render_template('update_book_alch.html', form=form)


@app.route('/delete_book/', methods=['POST'])
def delete_book():
    book_id = request.form['book.id']
    form = BookForm(request.form)
    form.delete(book_id)
    return redirect('/')


if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True
    )
