from faker import Faker
from settings import db


class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(125), nullable=False)
    books = db.relationship('Book', lazy='select', backref='author')

    def __repr__(self):
        return f'{self.id} Author name is {self.name}'


class Genre(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(125), nullable=False, unique=True)
    books = db.relationship('Book', lazy='select', backref='genre')

    def __repr__(self):
        return f'{self.id} Genre is {self.name}'


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    author_id = db.Column(db.ForeignKey('author.id'), nullable=False)
    genre_id = db.Column(db.ForeignKey('genre.id'), nullable=False)
    year = db.Column(db.String(4), nullable=False)

    def __repr__(self):
        return f'{self.id} Book name {self.name} | Author id {self.author} | Genre id {self.genre} | {self.year}'


db.create_all()


if __name__ == '__main__':

    fake = Faker(['en_GB', 'en_US'])
    genres = [
        'Autobiography', 'Biography', 'Cookbook', 'Classic', 'Comic book',
        'Dictionary', 'Crime', 'Encyclopedia', 'Drama', 'Guide',
        'Health', 'Fantasy', 'History', 'Graphic novel', 'Home and garden',
        'Humor', 'Horror', 'Mystery', 'Memoir', 'Philosophy',
        'Poetry', 'Romance', 'Satire', 'Short story', 'Science',
        'Suspense', 'Sports', 'Thriller', 'Western', 'Travel'
    ]

    for authors in range(25):
        author = Author(name=f'{fake.unique.name()}')
        db.session.add(author)

    for genre in genres:
        genre_name = Genre(name=genre)
        db.session.add(genre_name)

    for books in range(500):
        book = Book(name=f'{fake.sentence(nb_words=5)}',
                    author_id=fake.random_int(min=1, max=25),
                    genre_id=fake.random_int(min=1, max=len(genres)),
                    year=f'{fake.random_int(min=1950, max=2021)}')
        db.session.add(book)

    db.session.commit()
    db.session.remove()

    for instance in Author.query.all():
        print(instance)

    for instance in Genre.query.all():
        print(instance)

    for instance in Book.query.all():
        print(instance)
