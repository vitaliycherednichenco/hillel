import wtforms
from models import Author, Genre, Book
from settings import db


class AuthorForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Length(max=125, message="The name is too long.")],
                               label='Author name')

    def save(self):
        author = Author(name=self.name.data)
        db.session.add(author)
        db.session.commit()


class GenreForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Length(max=125, message="The genre is too long.")],
                               label='Name of the new genre')

    def save(self):
        genre = Genre(name=self.name.data)
        db.session.add(genre)
        db.session.commit()


class BookForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Length(max=255)],
                               label='Name of the book')
    author = wtforms.SelectField(validators=[wtforms.validators.InputRequired()],
                                 label='Author')
    genre = wtforms.SelectField(validators=[wtforms.validators.InputRequired()],
                                label='Genre')
    year = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Regexp('^(?:[0-2][0][0-2][0-1]|[0-2][0][0][0-9]|'
                                                                     '[1][0-9]{1,3}|[0-9]{1,3}|[0-9]{1,2}|[0-9]{1})$',
                                                                     message='Only numbers and no higher than 2021!'),
                                           wtforms.validators.Length(max=4)],
                               label='Year')

    def save(self):
        book = Book(name=self.name.data,
                    author_id=self.author.data,
                    genre_id=self.genre.data,
                    year=self.year.data)
        db.session.add(book)
        db.session.commit()

    def update(self, book_id):
        Book.query.filter_by(id=book_id).update({Book.name: self.name.data,
                                                 Book.author_id: self.author.data,
                                                 Book.genre_id: self.genre.data,
                                                 Book.year: self.year.data})
        db.session.commit()

    def delete(self, id):
        deleted_book = Book.query.get(id)
        db.session.delete(deleted_book)
        db.session.commit()
