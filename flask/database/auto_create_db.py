import sqlite3
from faker import Faker


def auto_create_db(name_db='library'):
    connect = sqlite3.connect(f'{name_db}.db')
    cursor = connect.cursor()
    fake = Faker(['en_GB', 'en_US'])
    genres = [
        'Autobiography', 'Biography', 'Cookbook', 'Classic', 'Comic book',
        'Dictionary', 'Crime', 'Encyclopedia', 'Drama', 'Guide',
        'Health', 'Fantasy', 'History', 'Graphic novel', 'Home and garden',
        'Humor', 'Horror', 'Mystery', 'Memoir', 'Philosophy',
        'Poetry', 'Romance', 'Satire', 'Short story', 'Science',
        'Suspense', 'Sports', 'Thriller', 'Western', 'Travel'
    ]

    cursor.execute(
        '''
        CREATE TABLE IF NOT EXISTS authors(
        author_id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL
        );'''
    )

    cursor.execute(
        '''
        CREATE TABLE IF NOT EXISTS genres(
        genre_id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT UNIQUE NOT NULL
        );'''
    )

    cursor.execute(
        '''
        CREATE TABLE IF NOT EXISTS books(
        book_id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,        
        author_id INTEGER NOT NULL,
        genre_id INTEGER NOT NULL,
        year INTEGER NOT NULL,
        FOREIGN KEY (author_id) REFERENCES authors (author_id),
        FOREIGN KEY (genre_id) REFERENCES genres (genre_id)
        );'''
    )

    for names in range(25):
        cursor.execute(
            f'''INSERT INTO authors(name)
            VALUES('{fake.unique.name()}');'''
        )

    for genre in genres:
        cursor.execute(
            f'''INSERT INTO genres(name)
            VALUES('{genre}');'''
        )

    for book in range(500):
        cursor.execute(
            f'''INSERT INTO books(name, author_id, genre_id, year )
            VALUES(
            '{fake.sentence(nb_words=5)}',
            {fake.random_int(min=1, max=25)},
            {fake.random_int(min=1, max=len(genres))},
            {fake.random_int(min=1950, max=2021)});'''
        )

    connect.commit()
    connect.close()


if __name__ == '__main__':
    auto_create_db()
