import wtforms
from models import Author, Genre, Book


class AuthorForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Length(max=125, message="The name is too long.")],
                               label='Author name')

    def save(self):
        Author.create(name=self.name.data)


class GenreForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Length(max=125, message="The genre is too long.")],
                               label='Name of the new genre')

    def save(self):
        Genre.create(name=self.name.data)


class BookForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Length(max=255)],
                               label='Name of the book')
    author = wtforms.SelectField(validators=[wtforms.validators.InputRequired()],
                                 label='Author')
    genre = wtforms.SelectField(validators=[wtforms.validators.InputRequired()],
                                label='Genre')
    year = wtforms.StringField(validators=[wtforms.validators.InputRequired(),
                                           wtforms.validators.Regexp('^(?:[0-2][0][0-2][0-1]|[0-2][0][0][0-9]|'
                                                                     '[1][0-9]{1,3}|[0-9]{1,3}|[0-9]{1,2}|[0-9]{1})$',
                                                                     message='Only numbers and no higher than 2021!'),
                                           wtforms.validators.Length(max=4)],
                               label='Year')

    def save(self):
        Book.create(name=self.name.data,
                    author=self.author.data,
                    genre=self.genre.data,
                    year=self.year.data)

    def update(self, book_id):
        (Book.update({Book.name: self.name.data,
                     Book.author: self.author.data,
                     Book.genre: self.genre.data,
                     Book.year: self.year.data}).where(Book.id == book_id)).execute()

    def delete(self, id):
        (Book.get(Book.id == id)).delete_instance()
