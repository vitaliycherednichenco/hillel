from peewee import SqliteDatabase, Model, AutoField, ForeignKeyField, CharField, DateField
from faker import Faker
                                                  #
                                                  # Вопрос по передаче аргумента в 5 строке.
db = SqliteDatabase('database/library_pw.db')     # Если передать database/library_pw.db и запустить models.py выдаст
                                                  # ошибку peewee.OperationalError: unable to open database file
                                                  # При передаче library_pw.db в папке database создастся файл
class Author(Model):                              # Но теперь когда запускаем приложение, базу не видно и для того
    id = AutoField(primary_key=True)              # что бы все работало нужно указать путь database/library_pw.db
    name = CharField(max_length=255, null=False)  #

    class Meta:
        database = db


class Genre(Model):
    id = AutoField()
    name = CharField(max_length=255, unique=True, null=False)

    class Meta:
        database = db


class Book(Model):
    name = CharField(max_length=255, null=False)
    author = ForeignKeyField(Author, backref='books', null=False)
    genre = ForeignKeyField(Genre, backref='books', null=False)
    year = DateField(null=False)

    class Meta:
        database = db


if __name__ == '__main__':
    db.connect()
    db.create_tables([Author, Genre, Book])

    ''' Данный способ наполнения базы работает значительно медленнее, чем auto_create_db.py'''

    fake = Faker(['en_GB', 'en_US'])
    genres = [
        'Autobiography', 'Biography', 'Cookbook', 'Classic', 'Comic book',
        'Dictionary', 'Crime', 'Encyclopedia', 'Drama', 'Guide',
        'Health', 'Fantasy', 'History', 'Graphic novel', 'Home and garden',
        'Humor', 'Horror', 'Mystery', 'Memoir', 'Philosophy',
        'Poetry', 'Romance', 'Satire', 'Short story', 'Science',
        'Suspense', 'Sports', 'Thriller', 'Western', 'Travel'
    ]

    for names in range(25):
        instance = Author(name=f'{fake.unique.name()}')
        instance.save()

    for genre in genres:
        instance = Genre(name=f'{genre}')
        instance.save()

    for books in range(500):
        instance = Book(name=f'{fake.sentence(nb_words=5)}',
                        author=fake.random_int(min=1, max=25),
                        genre=fake.random_int(min=1, max=len(genres)),
                        year=f'{fake.random_int(min=1950, max=2021)}')
        instance.save()

    db.close()
