import sqlite3
import requests
import json
from faker import Faker
from flask import Flask, render_template, request, redirect
app = Flask(__name__)


@app.route('/home/')
def render_home_page():
    return render_template('home.html')


@app.route('/requirements/')
def displays_file_content():
    with open('requirements.txt') as r:
        data = r.readlines()
        data = [line.strip('\n') for line in data]
        return render_template('package.html', packages=data)


@app.route('/generate-users/')
def generate_users():
    fake = Faker(['en_GB', 'ru_RU', 'de_DE', 'en_US', 'uk_UA'])
    count_of_users_to_display = 100
    if 'value' in request.values:
        count_of_users_to_display = abs(int(request.values['value']))
    names_and_emails = [fake.first_name() + ' ' + fake.company_email() for _ in range(100)]
    return render_template('user.html', users=names_and_emails[:count_of_users_to_display])


@app.route('/space/')
def men_in_space():
    data = requests.get('http://api.open-notify.org/astros.json')
    json_to_python = json.loads(data.text)
    return render_template('astronaut.html', men=json_to_python)


def execute_request(query):
    with sqlite3.connect('database/library.db') as conn:
        cursor = conn.cursor()
        cursor.execute(query)
        return cursor.fetchall()


@app.route('/')
def library():
    books = execute_request(
        f'''
        SELECT book_id, books.name, authors.name, genres.name, year, genre_id
        FROM books
        JOIN authors USING(author_id)
        JOIN genres USING(genre_id)
        ORDER BY book_id DESC;      
        '''
    )
    return render_template('library.html', books=books)


@app.route('/year/<int:year>/')
def books_by_year(year):
    books = execute_request(
        f'''
        SELECT book_id, books.name, authors.name, genres.name, year, genre_id
        FROM books
        JOIN authors USING(author_id)
        JOIN genres USING(genre_id)
        WHERE year = {year};
        '''
    )
    return render_template('books_by_year.html', books=books)


@app.route('/genre/<int:pk>/')
def books_by_genre(pk):
    books = execute_request(
        f'''
        SELECT book_id, books.name, authors.name, genres.name, year
        FROM books
        JOIN authors USING(author_id)
        JOIN genres USING(genre_id)
        WHERE genre_id = {pk};
        '''
    )
    return render_template('books_by_genre.html', books=books)


@app.route('/create/book/', methods=['GET', 'POST'])
def create_book():
    if request.method == 'POST':
        author_id = execute_request(
            f'''
            SELECT author_id 
            FROM authors
            WHERE name = '{request.form['author']}';
            '''
        )
        genre_id = execute_request(
            f'''
            SELECT genre_id 
            FROM genres
            WHERE name = '{request.form['genre']}';
            '''
        )
        execute_request(
            f'''
            INSERT INTO books(name, author_id, genre_id, year)
            VALUES(
            '{request.form['name']}',
            {author_id[0][0]},
            {genre_id[0][0]},
            {request.form['year']});
            '''
        )
        return redirect('/')
    authors = execute_request(
        f'''
        SELECT name
        FROM authors;
        '''
    )
    genres = execute_request(
        f'''
        SELECT name
        FROM genres;
        '''
    )
    return render_template('create_book.html', authors=authors, genres=genres)


@app.route('/create/author/', methods=['GET', 'POST'])
def create_author():
    if request.method == 'POST':
        execute_request(
            f'''
            INSERT INTO authors(name)
            VALUES('{request.form['name']}');
            '''
        )
        return redirect('/')
    return render_template('author.html')


@app.route('/create/genre/', methods=['GET', 'POST'])
def create_genre():
    if request.method == 'POST':
        list_of_genres = execute_request(
            f'''
            SELECT name
            FROM genres;
            '''
        )
        for genre in list_of_genres:
            if request.form['name'] == genre[0]:
                break
        else:
            execute_request(
                f'''
                INSERT INTO genres(name)
                VALUES('{request.form['name']}');
                '''
            )
            return redirect('/')
    return render_template('genre.html')


@app.route('/update/book/<int:book_id>/', methods=['GET', 'POST'])
def update_book(book_id):
    if request.method == 'POST':
        author_id = execute_request(
            f'''
            SELECT author_id
            FROM authors
            WHERE name = '{request.form['author']}';
            '''
        )
        genre_id = execute_request(
            f'''
            SELECT genre_id
            FROM genres
            WHERE name = '{request.form['genre']}';
            '''
        )
        execute_request(
            f'''
            UPDATE books
            SET
            name='{request.form['name']}',
            author_id={author_id[0][0]},
            genre_id={genre_id[0][0]},
            year={request.form['year']}
            WHERE book_id = {request.path.split('/')[-2]};
            '''
        )
        return redirect('/')
    authors = execute_request(
        f'''
        SELECT name
        FROM authors;
        '''
    )
    genres = execute_request(
        f'''
        SELECT name
        FROM genres;
        '''
    )
    book = execute_request(
        f'''
        SELECT books.name, authors.name, genres.name, year
        FROM books
        JOIN authors USING(author_id)
        JOIN genres USING(genre_id)
        WHERE book_id = {book_id};
        '''
    )
    return render_template('update_book.html', book=book[0], authors=authors, genres=genres)


@app.route('/delete_book/', methods=['POST'])
def delete_book():
    execute_request(
        f'''
        DELETE FROM books
        WHERE book_id={request.form['book_id']};
        '''
    )
    return redirect('/')


if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True
    )
