def escape_from_maze(maze: list, enter: list) -> list:

    aisle = '.'
    y, x = enter
    top_side = y == 0
    bottom_side = y == len(maze) - 1
    left_side = x == 0
    right_side = x == len(maze[0]) - 1          #
    taken_steps = [enter]                       # Переменная в которой сохраняем координаты точек в которых мы уже были.
    open_path = []                              # Переменная в которую добавляем координаты точек,
                                                # на которые можно перейти.
    if top_side and maze[y+1][x] == aisle:      #
        y += 1                                  # Определяем положение входа и смещаемся на один шаг.
        enter = [y, x]                          #
    elif bottom_side and maze[y-1][x] == aisle: # Это необходимо для предотвращения ошибок IndexError
        y -= 1                                  # Если вход будет внизу (справа) мы получим ошибку, так как
        enter = [y, x]                          # первым (третьим) проверяется условие приращения по 'y' ('x'),
    elif left_side and maze[y][x+1] == aisle:   # изменив порядок проверок мы можем избавиться от этой ошибки.
        x += 1                                  # Но тогда мы получим туже ошибку для входа сверху(слева).
        enter = [y, x]                          # Таким образом функция отрабатывает лишь половину возможных входов
    elif right_side and maze[y][x-1] == aisle:  # Так же позволяет получить правильное поведение в случае если
        x -= 1                                  # выход расположен напротив входа
        enter = [y, x]                          #

    taken_steps.append(enter)
    open_path.append(enter)                     #
                                                # В цикле проверяем, есть ли выход в соседних клетках. Если выход
    for coordinates in open_path:               # найден возвращаем координаты. Если нет добавляем те значения
        y, x = coordinates                      # в которых мы еще не были. Если цикл отработал и ничего не вернул
                                                # значит выход отсутствует.
        # if ((y == 0 or y == len(maze)-1) or (x == 0 or x == len(maze)-1)) and coordinates != enter:
        #     return coordinates

        if maze[y + 1][x] == aisle and [y + 1, x] not in taken_steps:
            if y + 1 == len(maze)-1:
                print('Exit coordinates -', [y + 1, x])                #
                return [y + 1, x]                                      # Проверку выполняем в каждом условии
            open_path.append([y + 1, x])                               # для предотвращения дополнительных вызовов
            taken_steps.append([y + 1, x])                             # цикла (которых может быть от 1 до 3)
        if maze[y - 1][x] == aisle and [y - 1, x] not in taken_steps:  # и заполнения двух списков не нужными данными,
            if y - 1 == 0:                                             # а не в начале или конце цикла как реализовано
                print('Exit coordinates -', [y - 1, x])                # на 31 строке.
                return [y - 1, x]                                      #
            open_path.append([y - 1, x])
            taken_steps.append([y - 1, x])
        if maze[y][x + 1] == aisle and [y, x + 1] not in taken_steps:
            if x + 1 == len(maze[0]) - 1:
                print('Exit coordinates -', [y, x + 1])
                return [y, x + 1]
            open_path.append([y, x + 1])
            taken_steps.append([y, x + 1])
        if maze[y][x - 1] == aisle and [y, x - 1] not in taken_steps:
            if x - 1 == 0:
                print('Exit coordinates -', [y, x - 1])
                return [y, x - 1]
            open_path.append([y, x - 1])
            taken_steps.append([y, x - 1])
    else:
        print('Exit not found')

maze = [
    '##########',
    '.........#',
    '######.###',
    '#......###',
    '#.######.#',
    '#........#',
    '##.#######',
    '##.##.####',
    '##......##',
    '#######.##',
]

maze_many_exits = [
    '##########',
    '.........#',
    '######.###',
    '#......###',
    '#.######.#',
    '#.........',
    '##.#######',
    '##.##.####',
    '##......##',
    '#######.##',
]

maze_no_exit = [
    '##########',
    '.........#',
    '######.###',
    '#......###',
    '#.######.#',
    '#........#',
    '##.#######',
    '##.##.####',
    '##......##',
    '##########',
]

maze_exit_opposite_the_enter = [
    '##########',
    '..........',
    '######.###',
    '#......###',
    '#.######.#',
    '#........#',
    '##.#######',
    '##.##.####',
    '##......##',
    '#######.##',
]

maze_enter_top = [
    '#.########',
    '#........#',
    '######.###',
    '#......###',
    '#.######.#',
    '#........#',
    '##.#######',
    '##.##.####',
    '##......##',
    '#######.##',
]

maze_enter_right = [
    '##########',
    '#.........',
    '######.###',
    '#......###',
    '#.######.#',
    '#........#',
    '##.#######',
    '##.##.####',
    '##.#######',
]

maze_not_square_by_x = [
    '##########################',
    '........#...............##',
    '######.##.#######.###.####',
    '#......##...#########....#',
    '#.######.##.##.......###.#',
    '#...........##.####.##...#',
    '##.###########...##....###',
    '##.##.##########.#########',
    '##......########..........',
    '##########################',
]

maze_not_square_by_y = [
    '##########################',
    '........#...............##',
    '######.##.#######.###.####',
    '#....#....#...#######....#',
    '#.##########.........###.#',
]

enter = [1, 0]
enter_top = [0, 1]
enter_right = [1, 9]
enter_bottom = [9, 7]
enter_into_the_maze = [1, 5]

if __name__ == '__main__':
    assert escape_from_maze(maze, enter) == [9, 7]
    assert escape_from_maze(maze_many_exits, enter) == [5, 9]
    assert escape_from_maze(maze_no_exit, enter) is None
    assert escape_from_maze(maze_exit_opposite_the_enter, enter) == [1, 9]
    assert escape_from_maze(maze_not_square_by_x, enter) == [8, 25]
    assert escape_from_maze(maze_not_square_by_y, enter) == [4, 24]
    assert escape_from_maze(maze, enter_into_the_maze) == [1, 0]
    assert escape_from_maze(maze_enter_top, enter_top) == [9, 7]
    assert escape_from_maze(maze_enter_right, enter_right) == [8, 2]
    assert escape_from_maze(maze, enter_bottom) == [1, 0]
