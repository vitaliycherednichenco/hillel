import asyncio
import os
import aiofiles
from time import time
from random import randint, uniform


SUM = '1'
MULTIPLYING = '2'
SUM_OF_SQUARE = '3'


def prepare_data(file_name='in_', file_count=10, quantity_numbers=10):
    start_time = time()

    for index in range(1, file_count + 1):
        with open(f'{file_name}{index}.dat', 'w') as file:
            action = randint(1, 3)
            numbers = [str(round(uniform(1, 10), 2)) for _ in range(quantity_numbers)]
            file.writelines([f'{action}\n', ' '.join(numbers)])

    print(f'Time of prepare data - {round((time() - start_time), 6)}')


def data_processing(files, result_file='aout.dat'):
    data = []
    for file in files:
        with open(file, 'r') as file:
            action = file.readline().strip()
            numbers = (float(number) for number in file.readline().split())

            if action == SUM:
                result = 0
                for digit in numbers:
                    result += digit
            elif action == MULTIPLYING:
                result = 1
                for digit in numbers:
                    result *= digit
            elif action == SUM_OF_SQUARE:
                result = 0
                for digit in numbers:
                    result += digit * digit
            else:
                result = 0
            data.append(result)
    result = sum(data)
    with open(result_file, 'w') as file:
        file.write(f'Sum - {result}')


async def async_data_processing(file):
    async with aiofiles.open(file, 'r') as file:
        lines = await file.readlines()
        action = lines[0].strip()
        numbers = (float(number) for number in lines[1].split())
        if action == SUM:
            result = 0
            for digit in numbers:
                result += digit
        elif action == MULTIPLYING:
            result = 1
            for digit in numbers:
                result *= digit
        elif action == SUM_OF_SQUARE:
            result = 0
            for digit in numbers:
                result += digit * digit
        else:
            result = 0
    return result


def main():
    start_time = time()
    files = [file for file in os.listdir() if file.startswith('in_')]
    data_processing(files)
    print(f'SEQ {round((time() - start_time), 6)}')


async def main_asy(result_file='aout_asy.dat'):
    start_time = time()
    files = [file for file in os.listdir() if file.startswith('in_') or file.startswith('/tmp')]
    tasks = [asyncio.create_task(async_data_processing(file)) for file in files]
    result = sum(await asyncio.gather(*tasks))
    async with aiofiles.open(result_file, 'w') as file:
        await file.write(f'Sum - {result}')
    print(f'ASY {round((time() - start_time), 6)}')


if __name__ == '__main__':
    start_time = time()
    # prepare_data()
    # main()
    # asyncio.run(main_asy())
    print(f'OUT {round((time() - start_time), 6)}')
