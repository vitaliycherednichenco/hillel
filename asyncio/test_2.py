import os
from os import listdir
from unittest import TestCase, IsolatedAsyncioTestCase
from unittest.mock import patch
from read_action_write import prepare_data, data_processing, async_data_processing, main, main_asy


class RAWTestCase(TestCase):

    def tearDown(self) -> None:
        [os.remove(file) for file in listdir() if file.startswith('in_test')]

    def test_data_processing(self):
        expected_value = 46.0
        for index in range(1, 4):
            with open(f'in_test_{index}.dat', 'w') as file:
                action = f'{index}\n'
                data = ' '.join(('2.0', '5.0'))
                file.writelines([action, data])
        files = ['in_test_1.dat', 'in_test_2.dat', 'in_test_3.dat']
        result_file = 'in_test_aout.dat'
        data_processing(files, result_file)
        with open(result_file, 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_sum(self):
        expected_value = 7.0
        with open('in_test.dat', 'w') as file:
            action = '1\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        data_processing(['in_test.dat'], 'in_test_aout.dat')
        with open('in_test_aout.dat', 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_multiplying(self):
        expected_value = 10.0
        with open('in_test.dat', 'w') as file:
            action = '2\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        data_processing(['in_test.dat'], 'in_test_aout.dat')
        with open('in_test_aout.dat', 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_sum_of_square(self):
        expected_value = 29.0
        with open('in_test.dat', 'w') as file:
            action = '3\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        data_processing(['in_test.dat'], 'in_test_aout.dat')
        with open('in_test_aout.dat', 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_wrong_action(self):
        with open('in_test.dat', 'w') as file:
            action = '123\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        data_processing(['in_test.dat'], 'in_test_aout.dat')
        self.assertNotIn(action.strip(), ['1', '2', '3'])

    def test_data_processing_result_wrong_action(self):
        expected_value = 0.0
        with open('in_test.dat', 'w') as file:
            action = '123\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        data_processing(['in_test.dat'], 'in_test_aout.dat')
        with open('in_test_aout.dat', 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    @patch('read_action_write.data_processing')
    def test_main(self, mock_data_processing):
        main()
        mock_data_processing.assert_called_once()

    @patch('read_action_write.data_processing')
    def test_main_files(self, mock_data_processing):
        main()
        files = [file for file in listdir() if file.startswith('in_')]
        mock_data_processing.assert_called_once_with(files)

    @patch('read_action_write.uniform', side_effect=(3.0, 6.0))
    @patch('read_action_write.randint', return_value=3)
    def test_prepare_data_action(self, mock_randint, mock_uniform):
        expected_value = 3
        prepare_data('in_test', 1, 2)
        with open('in_test1.dat', 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    @patch('read_action_write.uniform', side_effect=(3.0, 6.0))
    @patch('read_action_write.randint', return_value=3)
    def test_prepare_data_numbers(self, mock_randint, mock_uniform):
        expected_value = '3.0 6.0'
        prepare_data('in_test', 1, 2)
        with open('in_test1.dat', 'r') as file:
            value = file.readlines()[1]
        self.assertEqual(expected_value, value)


class AsyncTest(IsolatedAsyncioTestCase):

    def tearDown(self) -> None:
        [os.remove(file) for file in listdir() if file.startswith('in_test')]

    async def test_async_data_processing_sum(self):
        expected_value = 7.0
        with open('in_test.dat', 'w') as file:
            action = '1\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        value = await async_data_processing('in_test.dat')
        self.assertEqual(expected_value, value)

    async def test_async_data_processing_multiplying(self):
        expected_value = 10.0
        with open('in_test.dat', 'w') as file:
            action = '2\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        value = await async_data_processing('in_test.dat')
        self.assertEqual(expected_value, value)

    async def test_async_data_processing_sum_of_square(self):
        expected_value = 29.0
        with open('in_test.dat', 'w') as file:
            action = '3\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        value = await async_data_processing('in_test.dat')
        self.assertEqual(expected_value, value)

    async def test_async_data_processing_wrong_action(self):
        with open('in_test.dat', 'w') as file:
            action = '123\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        self.assertNotIn(action.strip(), ['1', '2', '3'])

    async def test_async_data_processing_result_wrong_action(self):
        expected_value = 0.0
        with open('in_test.dat', 'w') as file:
            action = '123\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        value = await async_data_processing('in_test.dat')
        self.assertEqual(expected_value, value)

    @patch('read_action_write.os.listdir', return_value=[f'in_test_1.dat',
                                                         f'in_test_2.dat',
                                                         f'in_test_3.dat'])
    async def test_main_asy(self, mock_os_listdir):
        expected_value = 46.0
        for index in range(1, 4):
            with open(f'in_test_{index}.dat', 'w') as file:
                action = f'{index}\n'
                data = ' '.join(('2.0', '5.0'))
                file.writelines([action, data])
        await main_asy('in_test_aout.dat')
        with open('in_test_aout.dat', 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)
