from os import listdir
from os.path import join
from unittest import TestCase, IsolatedAsyncioTestCase
from unittest.mock import patch
from testfixtures import TempDirectory
from read_action_write import prepare_data, data_processing, async_data_processing, main, main_asy


class RAWTestCase(TestCase):
    test_dir = TempDirectory()

    @classmethod
    def setUpClass(cls) -> None:
        for index in range(2, 5):
            with open(join(cls.test_dir.path, f'in_{index}.dat'), 'w') as file:
                action = f'{index - 1}\n'
                data = ' '.join(('2.0', '5.0'))
                file.writelines([action, data])

    @classmethod
    def tearDownClass(cls) -> None:
        cls.test_dir.cleanup()

    def test_data_processing(self):
        expected_value = 46.0
        files = [join(self.test_dir.path, file) for file in listdir(self.test_dir.path) if file.startswith('in_')]
        result_file = join(self.test_dir.path, 'aout.dat')
        data_processing(files, result_file)
        with open(result_file, 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_sum(self):
        expected_value = 7.0
        files = [join(self.test_dir.path, 'in_2.dat')]
        result_file = join(self.test_dir.path, 'aout.dat')
        data_processing(files, result_file)
        with open(result_file, 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_multiplying(self):
        expected_value = 10.0
        files = [join(self.test_dir.path, 'in_3.dat')]
        result_file = join(self.test_dir.path, 'aout.dat')
        data_processing(files, result_file)
        with open(result_file, 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_sum_of_square(self):
        expected_value = 29.0
        files = [join(self.test_dir.path, 'in_4.dat')]
        result_file = join(self.test_dir.path, 'aout.dat')
        data_processing(files, result_file)
        with open(result_file, 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    def test_data_processing_wrong_action(self):
        with open(join(self.test_dir.path, f'in_5.dat'), 'w') as file:
            action = '526\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        files = [join(self.test_dir.path, 'in_5.dat')]
        result_file = join(self.test_dir.path, 'aout.dat')
        data_processing(files, result_file)
        self.assertNotIn(action.strip(), ['1', '2', '3'])

    def test_data_processing_result_wrong_action(self):
        expected_value = 0.0
        with open(join(self.test_dir.path, f'in_5.dat'), 'w') as file:
            action = '526\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        files = [join(self.test_dir.path, 'in_5.dat')]
        result_file = join(self.test_dir.path, 'aout.dat')
        data_processing(files, result_file)
        with open(result_file, 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    @patch('read_action_write.data_processing')
    def test_main(self, mock_data_processing):
        main()
        mock_data_processing.assert_called_once()

    @patch('read_action_write.data_processing')
    def test_main_files(self, mock_data_processing):
        main()
        files = [file for file in listdir() if file.startswith('in_')]
        mock_data_processing.assert_called_once_with(files)

    @patch('read_action_write.uniform', side_effect=(3.0, 6.0))
    @patch('read_action_write.randint', return_value=3)
    def test_prepare_data_action(self, mock_randint, mock_uniform):
        expected_value = 3
        file_name = join(self.test_dir.path, 'in_')
        prepare_data(file_name, 1, 2)
        with open(join(self.test_dir.path, 'in_1.dat'), 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)

    @patch('read_action_write.uniform', side_effect=(3.0, 6.0))
    @patch('read_action_write.randint', return_value=3)
    def test_prepare_data_numbers(self, mock_randint, mock_uniform):
        expected_value = '3.0 6.0'
        file_name = join(self.test_dir.path, 'in_')
        prepare_data(file_name, 1, 2)
        with open(join(self.test_dir.path, 'in_1.dat'), 'r') as file:
            value = file.readlines()[1]
        self.assertEqual(expected_value, value)


class AsyncTest(IsolatedAsyncioTestCase):
    test_dir_asy = TempDirectory()

    @classmethod
    def setUpClass(cls) -> None:
        for index in range(2, 5):
            with open(join(cls.test_dir_asy.path, f'in_{index}.dat'), 'w') as file:
                action = f'{index - 1}\n'
                data = ' '.join(('2.0', '5.0'))
                file.writelines([action, data])

    @classmethod
    def tearDownClass(cls) -> None:
        cls.test_dir_asy.cleanup()

    async def test_async_data_processing_sum(self):
        expected_value = 7.0
        value = await async_data_processing(join(self.test_dir_asy.path, 'in_2.dat'))
        self.assertEqual(expected_value, value)

    async def test_async_data_processing_multiplying(self):
        expected_value = 10.0
        value = await async_data_processing(join(self.test_dir_asy.path, 'in_3.dat'))
        self.assertEqual(expected_value, value)

    async def test_async_data_processing_sum_of_square(self):
        expected_value = 29.0
        value = await async_data_processing(join(self.test_dir_asy.path, 'in_4.dat'))
        self.assertEqual(expected_value, value)

    async def test_async_data_processing_wrong_action(self):
        with open(join(self.test_dir_asy.path, f'in_5.dat'), 'w') as file:
            action = '526\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        self.assertNotIn(action.strip(), ['1', '2', '3'])

    async def test_async_data_processing_result_wrong_action(self):
        expected_value = 0.0
        with open(join(self.test_dir_asy.path, f'in_5.dat'), 'w') as file:
            action = '526\n'
            data = ' '.join(('2.0', '5.0'))
            file.writelines([action, data])
        value = await async_data_processing(join(self.test_dir_asy.path, 'in_5.dat'))
        self.assertEqual(expected_value, value)

    @patch('read_action_write.os.listdir', return_value=[f'{test_dir_asy.path}/in_2.dat',
                                                         f'{test_dir_asy.path}/in_3.dat',
                                                         f'{test_dir_asy.path}/in_4.dat'])
    async def test_main_asy(self, mock_os_listdir):
        expected_value = 46.0
        result_file = join(self.test_dir_asy.path, 'aout.dat')
        await main_asy(result_file)
        with open(result_file, 'r') as file:
            value = float(file.readline().split()[-1])
        self.assertEqual(expected_value, value)
