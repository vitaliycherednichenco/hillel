#!/bin/bash
app_name=""
#python=""

read -p "Enter the name of your application: " app_name
#read -p "Python interpreter path: " python
mkdir $app_name
cd $app_name
#$python -m venv env
python3.9 -m venv env
source env/bin/activate
pip install -U pip
pip install Django Pillow Faker requests
pip freeze > requirements.txt
django-admin startproject $app_name
cd $app_name
mv manage.py ../
rsync -a $app_name ../
rm -rf $app_name
echo ""
echo "Installed new django app $app_name!"
