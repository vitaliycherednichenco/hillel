import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'school.settings')
import django
django.setup()
from core.models import Group, Student, Teacher
from faker import Faker
from datetime import date


fake = Faker()

for person in range(100):
    birthday = fake.date_between(start_date='-60y', end_date='-20y')
    age = date.today().year - birthday.year
    student = Student.objects.create(
        first_name=f'{fake.first_name()}',
        last_name=f'{fake.last_name()}',
        email=f'{fake.email()}',
        phone=f'{fake.phone_number()}',
        birthday=f'{birthday}',
        age=f'{age}',
        address=f'{fake.address()}'
    )

teachers = {}

while len(teachers) != 7:
    for i in range(1, 16):
        group = fake.random_int(min=1, max=7)
        if group not in teachers:
            teachers[group] = [i]
        else:
            teachers[group].append(i)

groups = {}

for i in range(1, 16):
    groups[i] = []
    for key, value in teachers.items():
        if i in value:
            groups[i].append(key)

for value in teachers.values():
    birthday = fake.date_between(start_date='-45y', end_date='-30y')
    age = date.today().year - birthday.year
    teacher = Teacher.objects.create(
        first_name=f'{fake.first_name()}',
        last_name=f'{fake.last_name()}',
        email=f'{fake.email()}',
        phone=f'{fake.phone_number()}',
        birthday=f'{birthday}',
        age=f'{age}',
        company=f'{fake.company()}'
    )
    teacher.save()


for value in groups.values():
    group = Group(name=f'{fake.sentence(nb_words=5)}')
    group.save()
    group.teacher.set(value)
    for student in range(fake.random_int(min=10, max=20)):
        group.student.add(fake.random_int(min=8, max=100))

teacher_all = Teacher.objects.all()
index = 1
for teacher in teacher_all:
    teacher.group.set(teachers[index])
    index += 1
