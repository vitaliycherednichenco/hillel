from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView, CreateView
from django.forms.models import model_to_dict
from django.db.models.aggregates import Count, Avg, Max, Min
from django.db.models import IntegerField
from django.shortcuts import redirect
from core.models import Student, Teacher, Group
from core.forms import StudentForm, TeacherForm, GroupForm


class GroupListView(TemplateView):
    template_name = 'group_list.html'

    def get_context_data(self, **kwargs):
        context = super(GroupListView, self).get_context_data(**kwargs)
        groups = Group.objects.all().prefetch_related('teacher')
        context['groups'] = groups
        return context


class TeacherListView(TemplateView):
    template_name = 'teacher_list.html'

    def get_context_data(self, **kwargs):
        context = super(TeacherListView, self).get_context_data(**kwargs)
        groups = Teacher.objects.all().prefetch_related('group')
        context['groups'] = groups
        return context


class SortedGroupView(TemplateView):
    template_name = 'sorted_group.html'

    def get_context_data(self, **kwargs):
        context = super(SortedGroupView, self).get_context_data(**kwargs)
        groups = Group.objects.all().annotate(count=Count('student'),
                                              average=Avg('student__age', output_field=IntegerField()),
                                              max_age=Max('student__age'),
                                              min_age=Min('student__age'),
                                              ).prefetch_related('teacher')
        context['groups'] = groups
        return context


# class StudentCreateView(TemplateView):
#     template_name = 'create_person.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(StudentCreateView, self).get_context_data(**kwargs)
#         context['form'] = StudentForm()
#         return context
#
#     def post(self, request):
#         form = StudentForm(data=request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context)
#
#
# class TeacherCreateView(TemplateView):
#     template_name = 'create_person.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(TeacherCreateView, self).get_context_data(**kwargs)
#         context['form'] = TeacherForm()
#         return context
#
#     def post(self, request):
#         form = TeacherForm(data=request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context)
#
#
# class CreateGroupView(TemplateView):
#     template_name = 'create_group.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(CreateGroupView, self).get_context_data(**kwargs)
#         context['form'] = GroupForm()
#         return context
#
#     def post(self, request):
#         form = GroupForm(data=request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context)
#
#
# class StudentCreateView(FormView):
#     template_name = 'create_person.html'
#     form_class = StudentForm
#     success_url = '/'
#
#     def form_valid(self, form):
#         form.save()
#         return super(StudentCreateView, self).form_valid(form)
#
#
# class TeacherCreateView(FormView):
#     template_name = 'create_person.html'
#     form_class = TeacherForm
#     success_url = '/'
#
#     def form_valid(self, form):
#         form.save()
#         return super(TeacherCreateView, self).form_valid(form)
#
#
# class CreateGroupView(FormView):
#     template_name = 'create_group.html'
#     form_class = GroupForm
#     success_url = '/'
#
#     def form_valid(self, form):
#         form.save()
#         return super(CreateGroupView, self).form_valid(form)


class StudentCreateView(CreateView):
    template_name = 'create_person.html'
    model = Student
    form_class = StudentForm
    success_url = '/'


class TeacherCreateView(CreateView):
    template_name = 'create_person.html'
    model = Teacher
    form_class = TeacherForm
    success_url = '/'


class CreateGroupView(CreateView):
    template_name = 'create_group.html'
    form_class = GroupForm
    model = Group
    success_url = '/'


# class StudentUpdateView(TemplateView):
#     template_name = 'create_person.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(StudentUpdateView, self).get_context_data(**kwargs)
#         person_id = self.request.path.split('/')[-2]
#         person = Student.objects.get(id=person_id)
#         context['form'] = StudentForm(initial=model_to_dict(person))
#         return context
#
#     def post(self, request, pk):
#         form = StudentForm(data=request.POST)
#         if form.is_valid():
#             form.update(pk)
#             return redirect('/')
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context)
#
#
# class TeacherUpdateView(TemplateView):
#     template_name = 'create_person.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(TeacherUpdateView, self).get_context_data(**kwargs)
#         person_id = self.request.path.split('/')[-2]
#         person = Teacher.objects.get(id=person_id)
#         context['form'] = TeacherForm(initial=model_to_dict(person))
#         return context
#
#     def post(self, request, pk):
#         form = TeacherForm(data=request.POST)
#         if form.is_valid():
#             form.update(pk)
#             return redirect('/')
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context)
#
#
# class UpdateGroupView(TemplateView):
#     template_name = 'create_group.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(UpdateGroupView, self).get_context_data(**kwargs)
#         group_id = self.request.path.split('/')[-2]
#         group = Group.objects.get(id=group_id)
#         context['form'] = GroupForm(initial=model_to_dict(group))
#         return context
#
#     def post(self, request, pk):
#         form = GroupForm(data=request.POST)
#         if form.is_valid():
#             form.update(pk)
#             return redirect('/')
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context)


class StudentUpdateView(FormView):
    template_name = 'create_person.html'
    form_class = StudentForm
    success_url = '/'

    def get_initial(self):
        person_id = self.request.path.split('/')[-2]
        person = Student.objects.get(id=person_id)
        return model_to_dict(person)

    def form_valid(self, form):
        pk = self.request.path.split('/')[-2]
        form.update(pk)
        return super(StudentUpdateView, self).form_valid(form)

    # def get_form_kwargs(self):
    #     fkwargs = super(StudentUpdateView, self).get_form_kwargs()
    #     fkwargs['instance'] = Student.objects.get(id=self.request.path.split('/')[-2])
    #     return fkwargs


class TeacherUpdateView(FormView):
    template_name = 'create_person.html'
    form_class = TeacherForm
    success_url = '/'

    # def get_initial(self):
    #     person_id = self.request.path.split('/')[-2]
    #     person = Teacher.objects.get(id=person_id)
    #     return model_to_dict(person)

    def form_valid(self, form):
        pk = self.request.path.split('/')[-2]
        form.update(pk)
        return super(TeacherUpdateView, self).form_valid(form)

    def get_form_kwargs(self):
        fkwargs = super(TeacherUpdateView, self).get_form_kwargs()
        fkwargs['instance'] = Teacher.objects.get(id=self.request.path.split('/')[-2])
        return fkwargs


class UpdateGroupView(FormView):
    template_name = 'create_group.html'
    form_class = GroupForm
    success_url = '/'

    # def get_initial(self):
    #     group_id = self.request.path.split('/')[-2]
    #     group = Group.objects.get(id=group_id)
    #     return model_to_dict(group)

    def form_valid(self, form):
        pk = self.request.path.split('/')[-2]
        form.update(pk)
        return super(UpdateGroupView, self).form_valid(form)

    def get_form_kwargs(self):
        fkwargs = super(UpdateGroupView, self).get_form_kwargs()
        fkwargs['instance'] = Group.objects.get(id=self.request.path.split('/')[-2])
        return fkwargs

