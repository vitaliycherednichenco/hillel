from django import forms
from datetime import date
from core.models import Student, Teacher, Group


# class StudentForm(forms.Form):
#     first_name = forms.CharField(max_length=30)
#     last_name = forms.CharField(max_length=30)
#     email = forms.EmailField(max_length=125)
#     phone = forms.CharField(max_length=31)
#     birthday = forms.DateField()
#     age = forms.IntegerField()
#     address = forms.CharField(max_length=255)
#
#     def save(self):
#         Student.objects.create(**self.cleaned_data)
#
#     def update(self, pk):
#         Student.objects.filter(id=pk).update(**self.cleaned_data)
#
#
# class TeacherForm(forms.Form):
#     first_name = forms.CharField(max_length=30)
#     last_name = forms.CharField(max_length=30)
#     email = forms.EmailField(max_length=125)
#     phone = forms.CharField(max_length=31)
#     birthday = forms.DateField()
#     age = forms.IntegerField()
#     company = forms.CharField(max_length=255)
#     group = forms.ModelMultipleChoiceField(queryset=Group.objects.all())
#
#     def save(self):
#         group = self.cleaned_data.pop('group')
#         teacher = Teacher.objects.create(**self.cleaned_data)
#         teacher.save()
#         teacher.group.set(group)
#
    # def update(self, pk):
    #     group = self.cleaned_data.pop('group')
    #     teacher = Teacher.objects.get(id=pk)
    #     teacher.first_name = self.cleaned_data['first_name']
    #     teacher.last_name = self.cleaned_data['last_name']
    #     teacher.email = self.cleaned_data['email']
    #     teacher.phone = self.cleaned_data['phone']
    #     teacher.birthday = self.cleaned_data['birthday']
    #     teacher.age = self.cleaned_data['age']
    #     teacher.company = self.cleaned_data['company']
    #     teacher.group.set(group)
    #     teacher.save()
#
#
# class GroupForm(forms.Form):
#     name = forms.CharField(max_length=255)
#     teacher = forms.ModelMultipleChoiceField(queryset=Teacher.objects.all())
#     student = forms.ModelMultipleChoiceField(queryset=Student.objects.all())
#
#     def save(self):
#         teacher = self.cleaned_data.pop('teacher')
#         student = self.cleaned_data.pop('student')
#         group = Group.objects.create(**self.cleaned_data)
#         group.teacher.set(teacher)
#         group.student.set(student)
#
#     def update(self, pk):
#         teacher = self.cleaned_data.pop('teacher')
#         student = self.cleaned_data.pop('student')
#         group = Group.objects.get(id=pk)
#         group.name = self.cleaned_data['name']
#         group.teacher.set(teacher)
#         group.student.set(student)
#         group.save()


class StudentForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = '__all__'

    def update(self, pk):
        Student.objects.filter(id=pk).update(**self.cleaned_data)

    def clean_age(self):
        age = date.today().year - self.cleaned_data['birthday'].year
        age_from_form = self.cleaned_data['age']
        if age_from_form == age or age_from_form == age - 1:
            return self.cleaned_data['age']
        raise forms.ValidationError('Wrong age!')



class TeacherForm(forms.ModelForm):

    class Meta:
        model = Teacher
        fields = '__all__'

    def update(self, pk):
        group = self.cleaned_data.pop('group')
        teacher = Teacher.objects.get(id=pk)
        teacher.first_name = self.cleaned_data['first_name']
        teacher.last_name = self.cleaned_data['last_name']
        teacher.email = self.cleaned_data['email']
        teacher.phone = self.cleaned_data['phone']
        teacher.birthday = self.cleaned_data['birthday']
        teacher.age = self.cleaned_data['age']
        teacher.company = self.cleaned_data['company']
        teacher.group.set(group)
        teacher.save()

    def clean_age(self):
        age = date.today().year - self.cleaned_data['birthday'].year
        age_from_form = self.cleaned_data['age']
        if age_from_form == age or age_from_form == age - 1:
            return self.cleaned_data['age']
        raise forms.ValidationError('Wrong age!')


class GroupForm(forms.ModelForm):

    class Meta:
        model = Group
        fields = '__all__'

    def update(self, pk):
        teacher = self.cleaned_data.pop('teacher')
        student = self.cleaned_data.pop('student')
        group = Group.objects.get(id=pk)
        group.name = self.cleaned_data['name']
        group.teacher.set(teacher)
        group.student.set(student)
        group.save()


