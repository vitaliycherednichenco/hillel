from django.contrib import admin
from core.models import Student, Teacher, Group


admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Group)
