from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=125, blank=True, unique=True)
    phone = models.CharField(max_length=31)
    birthday = models.DateField()
    age = models.IntegerField()

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        abstract = True


class Student(Person):
    address = models.CharField(max_length=225)


class Teacher(Person):
    company = models.CharField(max_length=225)
    group = models.ManyToManyField('core.Group', related_name='groups')


class Group(models.Model):
    name = models.CharField(max_length=255)
    teacher = models.ManyToManyField(Teacher, related_name='teachers')
    student = models.ManyToManyField(Student, related_name='students')

    def __str__(self):
        return self.name
