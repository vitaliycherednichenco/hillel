"""school URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.urls import path, include
from core.views import GroupListView, TeacherListView, SortedGroupView,\
    StudentCreateView, TeacherCreateView, CreateGroupView,\
    StudentUpdateView, TeacherUpdateView, UpdateGroupView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('groups/', GroupListView.as_view()),
    path('teachers/', TeacherListView.as_view()),
    path('sorted/', SortedGroupView.as_view()),
    path('new_student/', StudentCreateView.as_view()),
    path('new_teacher/', TeacherCreateView.as_view()),
    path('new_group/', CreateGroupView.as_view()),
    path('update_student/<int:pk>/', StudentUpdateView.as_view()),
    path('update_teacher/<int:pk>/', TeacherUpdateView.as_view()),
    path('update_group/<int:pk>/', UpdateGroupView.as_view()),
    path('__debug__/', include(debug_toolbar.urls)),
]
