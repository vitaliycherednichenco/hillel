def parse(query: str) -> dict:
    # Solution with dictionary generator
    formatted_query = (query.strip('&?').replace('&', '?').replace('=', '?').split('?'))[1:]
    return {k: v for (k, v) in zip(formatted_query[::2], formatted_query[1::2])}

    # Solution with cycle for
    # formatted_query = (query.strip('&?').replace('&', '?').replace('=', '?').split('?'))[1:]
    # result = {}
    # for i in range(0, len(formatted_query), 2):
    #     result[formatted_query[i]] = formatted_query[i+1]
    # return result


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}


def parse_cookie(query: str) -> dict:
    if query == '':
        return {}
    formatted_query = query.strip(';').split(';')
    result = {}
    for item in formatted_query:
        result[item.split('=', 1)[0]] = item.split('=', 1)[1]
    return result
    #
    # if query == '':
    #     return {}
    # formatted_query = query.strip(';').split(';')
    # list_of_couple_cookie = []
    # for item in formatted_query:
    #     list_of_couple_cookie.append(tuple(item.split('=', 1)))
    # return dict(list_of_couple_cookie)
    #
    # Solution in one line
    # return {k.split('=', 1)[0]: k.split('=', 1)[1] for k in query.strip(';').split(';') if query != ''}


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}
